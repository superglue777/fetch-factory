/**
 * IndexDb store operations
 */

import BaseStore from "./base-store";

// Primary db target is indexedDB
const indexedDb =
  window.indexedDB ||
  window.mozIndexedDB ||
  window.webkitIndexedDB ||
  window.msIndexedDB;

export default class IndexDbStore extends BaseStore {
  /**
   * @param {string} dbName
   * @param {string} collectionName
   * @param {string} idKeyPath path to a unique key in your data
   */
  constructor(dbName, collectionName, idKeyPath) {
    this._dbName = dbName;
    this._collectionName = collectionName;
    this._idKeyPath = idKeyPath;
    this._dbRef = null;
    this._openIndexDb();
  }

  /**
   * Write to key data pair to indexDb
   * @param {string} namespace
   * @param {string} key
   * @param {any} json
   */
  async Write(namespace, key, json) {
    try {
      const db = await this._openIndexDb();
      this._writeIndexDb(db, this._keyPath(namespace, key), json);
    } catch (error) {
      console.error("IndexDb write error", error);
    }
  }

  /**
   * Read data by key from indexDb
   * @param {string} namespace
   * @param {string} key
   * @param {any} defaultValue
   * @return {any}
   */
  async Read(namespace, key, defaultValue = null) {
    try {
      const db = await this._openIndexDb();
      return await this._readIndexDb(db, this._keyPath(namespace, key));
    } catch (error) {
      console.error("IndexDb read error", error);
      return defaultValue;
    }
  }

  /**
   * Initialize database with a new object store
   * @private
   * @param {object} db
   */
  _initDb(db) {
    db.createObjectStore(this._collectionName, { keyPath: this._idKeyPath });
  }

  /**
   * Return database ref. Open database if needed.
   * @private
   * @return {object}
   */
  async _openIndexDb() {
    if (this._dbRef) return this._dbRef;

    return new Promise((resolve, reject) => {
      let request;
      try {
        request = indexedDb.open(this._dbName);
      } catch (error) {
        console.error("Failed to open IndexDb", error);
        return reject(error);
      }

      request.onerror = event => reject(event);

      request.onsuccess = event => {
        this._dbRef = event.target.result;
        resolve(this._dbRef);
      };

      request.onupgradeneeded = event => this.initDb(event.target.result);
    });
  }

  /**
   * Write key data pair to IndexDb
   * @private
   * @param {object} db database ref
   * @param {string} key
   * @param {object} data
   */
  _writeIndexDb(db, key, data) {
    const tx = db.transaction(this._collectionName, "readwrite");
    const store = tx.objectStore(this._collectionName);
    store.put({ key, data });
  }

  /**
   * Read data by key from IndexDb
   * @private
   * @param {object} db database ref
   * @param {string} key
   * @return {object}
   */
  _readIndexDb(db, key) {
    return new Promise((resolve, reject) => {
      const tx = db.transaction(STORE_NAME, "readonly");
      const store = tx.objectStore(STORE_NAME);
      const request = store.get(key);

      request.onsuccess = event =>
        resolve(request.result && request.result.data);

      request.onerror = event => reject(event);
    });
  }

  /**
   * Build key path for IndexDb
   * @private
   * @param {string} namespace
   * @param {string} key
   * @return {string}
   */
  _keyPath(namespace, key) {
    return `${namespace}.${key || ""}`;
  }
}
