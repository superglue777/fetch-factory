/**
 * Local Store operations
 */

const localStore = window.localStorage;

export default class LocalStore {
  /**
   * @param {string} dbName
   * @param {string} collectionName
   */
  constructor(dbName, collectionName) {
    this._dbName = dbName;
    this._collectionName = collectionName;
  }

  /**
   * Write to key data pair to localStore
   * @param {string} namespace
   * @param {string} key
   * @param {any} json
   */
  async Write(namespace, key, json) {
    try {
      const data = JSON.stringify({ json });
      localStore.setItem(this._keyPath(namespace, key), data);
    } catch (error) {
      console.error("LocalStore write error", error);
    }
  }

  /**
   * Read data by key from localStore
   * @param {string} namespace
   * @param {string} key
   * @param {any} defaultValue
   * @return {any}
   */
  async Read(namespace, key, defaultValue = null) {
    try {
      const data = localStore.getItem(this._keyPath(namespace, key));
      return JSON.parse(data).json;
    } catch (error) {
      console.error("LocalStore read error", error);
      return defaultValue;
    }
  }

  /**
   * Build key path for LocalStore
   * @private
   * @param {string} namespace
   * @param {string} key
   * @return {string}
   */
  _keyPath(namespace, key) {
    return `${this._dbName}.${namespace}.${key || ""}`;
  }
}
