/**
 * Local Store management routines
 * Uses IndexDb if available, otherwise LocalStore
 */

import LocalStore from './stores/local-store';
import IndexDbStore from './stores/indexDb-store';

const indexedDb =
  window.indexedDB ||
  window.mozIndexedDB ||
  window.webkitIndexedDB ||
  window.msIndexedDB;

export default class LocalStoreManager {
  /**
   * @param {string} dbName
   * @param {string} collectionName
   * @param {string} idKeyPath path to a unique key in your data
   */
  constructor(dbName, collectionName, idKeyPath) {
    this._driver = indexedDb
      ? new IndexDbStore(dbName, collectionName, idKeyPath)
      : new LocalStore(dbName, collectionName);
  }

  /**
   * Write to key data pair to indexDb or localStore
   * @param {string} namespace
   * @param {string} key
   * @param {any} json
   */
  async Write(namespace, key, json) {
    await this._driver.Write(namespace, key, json);
  }

  /**
   * Read data by key from indexDb or localStore
   * @param {string} namespace
   * @param {string} key
   * @param {any} defaultValue
   * @return {Promise<any>}
   */
  async Read(namespace, key, defaultValue = null) {
    return this._driver.Read(namespace, key, defaultValue);
  }
}
