import { Observable } from 'rxjs';
import { ActionsObservable } from 'redux-observable';
import LocaStoreManager from './local-store-manager';

/**
 * Factory for simplifying creation of actions, reducers, epics and selectors
 * for basic read operations.
 *
 * Supported actions: fetch, fetched, refresh, cancel, failed.
 *
 * @example
 *
 *  ===========================================================================
 *  @ clients/redux/fetch.myData.js
 *  // this is example of custom module for specific component
 *
 *  import FetchFactory from '../utils/FetchFactory';
 *  import myApi from '../clients/myApi';
 *
 *  const pFetch = action => {
 *      return myApi.getMyData(action.params)
 *  }
 *
 *  const myNameSpace = "MyData";
 *  const Factory = new FetchFactory(myNameSpace);
 *
 *  export default {
 *    actions: Factory.actions,
 *    reducer: Factory.createReducer([]),
 *    epic: Factory.createEpic(pFetch),
 *    selectors: Factory.createSelectors(),
 *    nameSpace
 *  };
 *
 *
 *  ===========================================================================
 *  @ /rootEpic.js
 *  // this is example of rootEpic.js
 *
 *  import { combineEpics } from 'redux-observable';
 *  import myData from './services/redux/fetch.myData';
 *  export const rootEpic = combineEpics(myData.epic, epic1, epic2, ...);
 *
 *  ===========================================================================
 *  @ /rootReducer.js
 *
 *  import myData from './services/redux/fetch.myData';
 *  ....
 *  export default combineReducers({
 *      [myData.nameSpace]: myData.reducer
 *      ....
 *  })
 *
 *  ===========================================================================
 *  @ .../index.js - component main file
 *
 *  import { myActions, mySelector } from '.../services/redux/fetch.myData';
 *  .....
 *
 *  componentWillUnmount() {
 *      dispatch(myActions.cancel(this.props.componentId))
 *  }
 *
 *  // dispatching fetch request
 *
 *      dispatch(myActions.fetch(this.props.componentId))
 *
 *  .....
 *
 *  function mapStateToProps(state, props) {
 *      const myData = mySelector(state, props.componentId)
 *      return {
 *          myData
 *      };
 *  }
 */

const CACHE_DB_NAME = 'fetch-factory-db';
const CACHE_COLLECTION_NAME = 'fetch-factory-col';
const CACHE_KEY_PATH = 'key';

export default class FetchFactory {
  /**
   * Create factory
   * @param {string} nameSpace unique prefix for action type names
   * @param {object} [options] object with options
   *   {
   *             resetOnFetch -  if true, new [fetch] actions "cleans"
   *                             current state to defaultDataState,
   *                             default is false
   *             useLocalCache - if true, data is cached in local browser
   *                             storage, between sessions on first [fetch]
   *                             request data is imediately loaded from
   *                             browser storage and updated on [fetched]
   *      }
   */
  constructor(nameSpace, options = {}) {
    this._nameSpace = nameSpace;
    this._useLocalCache = options.useLocalCache || false;
    this._resetOnFetch = options.resetOnFetch || false;

    if (this._useLocalCache) {
      this._localStoreManager = new LocaStoreManager(
        CACHE_DB_NAME,
        CACHE_COLLECTION_NAME,
        CACHE_KEY_PATH,
      );
    }

    // initialize actionTypes
    this.actionTypes = {
      fetch: `${nameSpace}_FETCH`.toUpperCase(),
      fetched: `${nameSpace}_FETCHED`.toUpperCase(),
      refresh: `${nameSpace}_REFRESH`.toUpperCase(),
      cancel: `${nameSpace}_CANCEL`.toUpperCase(),
      failed: `${nameSpace}_FAILED`.toUpperCase(),
    };

    this.actions = this.createActions();
  }

  /**
   * Create set of actions
   * @return {object} object properties corresponds to action creation functions
   */
  createActions() {
    return {
      fetch: (params, id) => ({
        type: this.actionTypes.fetch,
        id,
        params,
      }),
      fetched: (data, id) => ({
        type: this.actionTypes.fetched,
        id,
        data,
      }),
      refresh: id => ({
        type: this.actionTypes.refresh,
        id,
      }),
      cancel: id => ({
        type: this.actionTypes.cancel,
        id,
      }),
      failed: (error, id) => ({
        type: this.actionTypes.failed,
        id,
        error,
      }),
    };
  }

  /**
   * Create reducer function
   * @param {any} defaultDataState default state
   * @param {function} [mergeState] option function that performs some custom
   *        merging of current and new state fefault function just writes new
   *        state over existing
   */
  createReducer(
    defaultDataState,
    mergeState = (currentState, newState) => newState,
  ) {
    // actual reducer logic
    // we will use it at the end of createReducer
    this.defaultDataState = defaultDataState;
    const reduce = (
      state = {
        isFetching: false,
        isVirgin: true,
        error: null,
        data: defaultDataState,
      },
      action,
    ) => {
      const { actionTypes } = this;

      switch (action.type) {
        case actionTypes.fetch: {
          const update = {
            isFetching: true,
            lastParams: action.params,
            error: null,
          };
          if (this._resetOnFetch) {
            update.data = defaultDataState;
          }
          return Object.assign({}, state, update);
        }

        case actionTypes.fetched: {
          if (this._useLocalCache) {
            this._localStoreManager.Write(
              this._nameSpace,
              action.id,
              action.data,
            );
          }

          return Object.assign({}, state, {
            isFetching: false,
            isVirgin: false,
            error: null,
            data: mergeState(state, action.data),
          });
        }

        case actionTypes.cancel:
          return Object.assign({}, state, {
            isFetching: false,
            isVirgin: false,
            error: null,
          });

        case actionTypes.failed:
          return Object.assign({}, state, {
            isFetching: false,
            isVirgin: false,
            error: action.error,
          });

        default:
          return state;
      }
    };

    // this is actual reducer function returned by createReducers()
    return (currentState = null, action) => {
      // check if it is our action
      const {
        actionTypes: { fetch, fetched, cancel, failed },
      } = this;
      if ([fetch, fetched, cancel, failed].indexOf(action.type) < 0) {
        return currentState;
      }

      if (typeof action.id === 'undefined' || action.id === null) {
        return reduce(currentState === null ? undefined : currentState, action);
      }

      const state = currentState === null ? {} : currentState;
      const stateShallowCopy = Object.assign({}, state);
      stateShallowCopy[action.id] = reduce(state[action.id], action);
      return stateShallowCopy;
    };
  }

  /**
   * Create selector function
   */
  createSelectors() {
    /**
     * Get our state 'slot' in redux store
     */
    const getSlot = (state, id) => {
      const slot = state && state[this._nameSpace];
      if (typeof id === 'undefined' || id === null) {
        return slot;
      }

      return slot && slot[id];
    };

    /**
     * Selector for data retrieval
     */
    const data = (state, id) => {
      const dt = getSlot(state, id);
      return dt && typeof dt.data !== 'undefined'
        ? dt.data
        : this.defaultDataState;
    };

    /**
     * Selector to check fetching status
     */
    const isFetching = (state, id) => {
      const dt = getSlot(state, id);
      return (dt && dt.isFetching) || false;
    };

    /**
     * Selector to check if there was completed at least one fetch
     */
    const isVirgin = (state, id) => {
      const dt = getSlot(state, id);
      return dt ? dt.isVirgin : true;
    };

    /**
     * Selector to get params of the last fetch action
     */
    const lastParams = (state, id) => {
      const dt = getSlot(state, id);
      return dt && dt.lastParams;
    };

    /**
     * Selector to check if fetching error ocured
     * @param {object} state root Redux state
     * @param {string} [id] in case our component is not "singletone" and can be
     *        in several instances at once we provide component unique key,
     *        to access its data
     */
    const error = (state, id) => {
      const dt = getSlot(state, id);
      return dt && dt.error;
    };

    // store selectors for later use in other methods
    this.selectors = {
      data,
      isFetching,
      isVirgin,
      lastParams,
      error,
    };
    return this.selectors;
  }

  /**
   * Creates epic function
   * @param {function} pFetch actual fetching function returning promise,
   *        resolved after call to api or etc.
   */
  createEpic(pFetch) {
    const { fetch, refresh, cancel } = this.actionTypes;
    const { fetched, failed } = this.actions;

    // epic responsible for [fetch -> fetched, cancel, failed] flow
    const fetchSubEpic = (action$, store) =>
      action$
        .ofType(fetch)
        // fetch data and produce "fetched" action
        .mergeMap(action => {
          return (
            Observable.from(
              pFetch(action.params, action.id).then(result =>
                fetched(result, action.id),
              ),
            )
              // prevent fetch results in case of "cancel" action,
              // or in case of next "fetch" action with the same component "id"
              .takeUntil(
                action$
                  .ofType(cancel)
                  .filter(
                    cancelAction =>
                      action.id &&
                      cancelAction.id &&
                      cancelAction.id === action.id,
                  ),
              )
              // eslint-disable-next-line arrow-parens
              .catch(err => Observable.of(failed(err, action.id)))
          );
        });

    // epic responsible for initial data loading when localStore cache is in use
    const preloadSubEpic = (action$, store) =>
      action$
        .ofType(fetch)
        .filter(action => {
          if (this._useLocalCache) {
            return this.selectors.isVirgin(store.getState(), action.id);
          }
          return false;
        })
        .mergeMap(
          action =>
            Observable.from(
              this._localStoreManager
                .Read(this._nameSpace, action.id)
                .then(data => {
                  if (data) {
                    return this.actions.fetched(data, action.id);
                  }
                  return Promise.reject(null);
                }),
            ).catch(() => Observable.empty()), // ignore rejected promises
        );

    // epic responsible for [refresh -> fetch] flow
    const refreshSubEpic = (action$, store) =>
      action$
        .ofType(refresh)
        // check if refresh comes before first fetch
        // we can process refresh only if fetch was reduced at least once
        // isVirgin indicates that
        .filter(action => !this.selectors.isVirgin(store.getState(), action.id))
        // fetch data and produce "fetched" action
        .mergeMap(action => {
          const lastParams = this.selectors.lastParams(
            store.getState(),
            action.id,
          );
          return Observable.of(this.actions.fetch(lastParams, action.id));
        });

    return ($action, store) => {
      const groupedActions = $action
        .groupBy(action =>
          typeof action.id === 'undefined' ? null : action.id,
        )
        // Obaservable having stream of splited ActionObservable objects
        .flatMap(group => {
          // ActionObservable taken from the higher order stream
          const groupObservable = new ActionsObservable(group);
          return Observable.merge(
            // cached values preload flow
            preloadSubEpic(groupObservable, store),
            // regular fetch flow
            fetchSubEpic(groupObservable, store),
            // refresh flow (produces fetch with the last params)
            refreshSubEpic(groupObservable, store),
          );
        });
      return groupedActions;
    };
  }
}
