/**
 * Abstract class for store operations
 */

export default class BaseStore {
  /**
   * Write to key data pair to indexDb or localStore
   * @abstract
   * @param {string} namespace
   * @param {string} key
   * @param {any} json
   */
  async Write(namespace, key, json) {}

  /**
   * Read data by key from indexDb or localStore
   * @abstract
   * @param {string} namespace
   * @param {string} key
   * @param {any} defaultValue
   * @return {any}
   */
  async Read(namespace, key, defaultValue = null) {}
}
