Factory for simplifying creation of actions, reducers, epics and selectors for
basic read operations.
It is as easy as just providing namespace and fetch function. That's it.

**NOTE**: this is excerpt from full scale SAAS project and is not intended for
use as is. This factory proved to save lots of dev hours when implementing
simple data fetch from BE API to redux store. Produced selectors can be used
directly or for Reselect to make more complicated data relations.

**SEE** `index.js` for main logic and **usage examples**

Supported actions: `fetch`, `fetched`, `refresh`, `cancel`, `failed`.

Supported features:
  
*  **Caching data in browser storage**. If enabled, first fetch instantly loads
data from the cache and writes to redux store making it available for components
to display. After async fetch is completed, newly arrived data is saved to cache
and to redux store. This is usefull for fast loading rarely updated data.

*  **Store reset on fetch**. If enabled, resets redux to default value and then
performs fetch and saves arrived data to redux.

*  **Multiple instances** of data in redux store. So that several compoents can
use the same fetcher with isolated redux stores. Just provide unique component
id when dispatching actions.

*  **Handling of interleaved actions** in fetch->fetched flow (mergeMap). Only
the latest fetch survives the results.

*  **Cancelation of action**. Used for gracefull unmounting of components

*  **Refresh action**. This will refetch data with parameters from the last
fetch

 Helper state props:
      
*  `isFetching` - true, while fetch is in progress
      
*  `isVirgin` - true, until fetch is completed, failed or canceled
      
*  `error` - fetch error